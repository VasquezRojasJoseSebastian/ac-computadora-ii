# Mapa 1 Computadoras electricas 
```plantuml
@startmindmap
* Computadoras electricas
** Computadoras electromecánicas
*** Computadora más grande
**** Harvard Mark1 (IBM 1994 )
***** Especificaciones
****** 765,000 componentes
****** 80km de cable
****** Eje de 15m
****** Motor 5hp
***** Usos 
****** Proyecto Manhattan 
***** Operaciones
****** 3 sumas o restas por segundo
****** 1 multiplicación en 6 segundos
****** 1 división en 15 segundos 
****** 1 operación compleja varios minutos
**** Rele
***** Interruptor mecánico controlado eléctricamente
***** Componentes
****** Bobina 
****** Brazo de hierro móvil 
****** 2 contactos
***** Funcionamiento
****** Cuando una corriente eléctrica atraviesa la bobina genera un campo magnético que atrae al brazo mecánico y este a su vez une los contactos cerrando el circuito 
***** Cambio de estado 
****** 50 Hertz en 1940
** Computadoras eléctricas 
*** Colossus Mark 1
**** Componentes: 1600 tubos de vacio
**** Primera computadora electrónica programable
**** Se construyeron cerca de 10 colossus 
*** Eniac
**** Calculadora 
**** Integradora
**** Numérica
**** Electrónica
**** Primera computadora electrónica programable de propósito general
**** 5000 sumas y restas de 10 digitos por segundo 
*** Tubos de vacío
**** Componentes 
***** 1 filamento 
***** 2 electrodos
***** 1 bulbo de cristal sellado
**** Ventajas
***** Mismo uso que los relés sin partes móviles
***** Mayor confiabilidad 
***** Se dañaban menos con el uso
**** Usos
***** Bases de la radio	
***** Teléfonos a larga distancia 	
***** Otros dispositivos electrónicos a lo largo de 50 años 
*** Transistor
**** Es un interruptor eléctrico
**** Cumple la misma función que el relé y el tubo de vacío
**** Ventajas 
***** Cambio de estado 10,000 Hertz
***** Mas pequeños que los relés y tubos de vacío
***** Eran solidos 
**** Hoy en día su tamaño es menor a 50nm y su cambio de estado 1,000,000 Hertz 
*** Primer computador con transistores
**** 4500 sumas o restas por segundo
**** 80 divisiones o multiplicaciones por segundo 
@endmindmap
```
# Mapa 2 Arquitectura Von Neumann y Arquitectura Harvard
```plantuml
@startmindmap
* Arquitectura Von Neumann y Arquitectura Harvard
** Ley de Moore
*** Establece que la velocidad del procesador o el poder de procesamientos se duplica cada doce meses
*** Electrónica 
**** El número de transistores se duplica cada año
**** El costo del chip permanece sin cambios
**** Cada 18 meses se duplica la potencia de cálculo sin modificar el costo
*** Performance
**** Se incrementa la velocidad del procesador
**** Se incrementa la capacidad de la memoria
**** La velocidad de la memoria corre siempre por detrás de la velocidad del procesador
** Arquitectura Von Neumann
*** Partes fundamentales 
**** Célula o unidad central de procesamiento 
**** La memoria principal
**** El modulo o sistema de entrada y salida
*** Modelo 
**** Los datos y programas se almacenan en una misma memoria de lectura/escritura 
**** Los contenidos de esta memoria se acceden indicando su posición sin importar su tipo
**** Ejecución en secuencia (salvo que se indique lo contrario)
**** Representación binaria 
*** Contiene 
**** Unidad central de procesamiento la cual contiene
***** Unidad de control 
***** Unidad aritmética lógica y registros
**** Memoria principal la cual almacena datos e instrucciones 
**** Sistema de entrada y salida
*** Programa almacenado
*** Cuello de botella 
**** Cuando la cantidad de datos que pasa entre dos elementos difiere mucho en tiempo con las de velocidades de ellos por lo cual la CPU puede permanecer ociosa.	
*** Interconexión 
**** Todos los componentes se comunican a través de un sistema de buses.
**** Bus de datos 
**** Bus de direcciones
**** Bus de control
** Arquitectura Harvard
*** Modelo
**** Arquitectura de computadoras que utilizan dispositivos de almacenamiento para las instrucciones y otro para los datos.
*** Contiene
**** Unidad central de procesamiento la cual contiene
***** Unidad de control
***** Unidad aritmético lógica 
**** Registros
*** Memorias 
**** Las más rápidas tienen un costo alto. La solución es proporcionar una pequeña cantidad de memoria muy rápida conocida como CACHE. Mientras los datos que necesita el procesador esten en el cache el rendimiento será mayor
*** 
**** Las instrucciones y datos se almacenan en caches separadas para mejorar el rendimiento. Esto ocasiona tener que dividir la cantidad de cache entre los dos por lo que funciona mejor solo cuando la frecuencia de lectura de las instrucciones y de datos es aproximadamente la misma
**** Esto arquitectura se utiliza en PIC’s o en microcontroladores usados habitualmente en productos de propósito especifico.
*** Procesador 
**** Unidad de control UC
**** Unidad aritmético lógica ALU
**** La UC lee la instrucción de la memoria de instrucciones, genera las señales de control para obtener los operandos y luego lo ejecuta mediante la ALU y lo almacena en la memoria de datos
*** Memoria de instrucciones 
**** Almacena las instrucciones 
*** Memoria de datos 
**** Se almacenan los datos utilizados por los programas

@endmindmap
```
